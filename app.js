const bodyParser = require('body-parser');
const express = require('express');
const logger = require('morgan');
const mongoClient = require('mongoose');

mongoClient
  .connect('mongodb://localhost/nodejsapi', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('ok'))
  .catch((err) => console.error('not ok', err));

const app = express();
const usersRoute = require('./routes/user');

// Middlewares
app.use(logger('dev'));
app.use(bodyParser.json());

// Route
app.use('/users', usersRoute);

// Catch 404 error
app.use((req, res, next) => {
  const error = new Error('Not Found');
  error.status = 404;
  next(error);
});

// Error hander function
app.use((err, req, res, next) => {
  const error = app.get('env') === 'development' ? err : {};
  const status = error.status || 500;

  // res to client
  return res.status(status).json({ error: { message: error.message } });
});

// Start the server

const port = app.get('port') || 3000;
app.listen(port, () => {
  console.log('Server listening on port ' + port);
});
