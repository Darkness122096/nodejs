const User = require('../models/UserModel');

const index = async (req, res, next) => {
  const user = await User.User.find({});
  return res.status(200).json({ user });
};

const newUser = async (req, res, next) => {
  const newUser = new User.User(req.body);
  await newUser.save();
  res.status(201).json({ user: newUser });
};

module.exports = {
  index,
  newUser,
};
